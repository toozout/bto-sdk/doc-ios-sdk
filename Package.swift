// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "tooz-iOS-SDK",
    platforms: [
        .iOS(.v11),
        .watchOS(.v6)
    ],
    products: [
        .library(
            name: "tooz-iOS-SDK",
            targets: ["tooz-iOS-SDK"]),
    ],
    targets: [
        .binaryTarget(name: "tooz-iOS-SDK", path: "tooz-iOS-SDK.xcframework")
    ]
)
