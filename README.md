# tooz iOS SDK 

## 1. Introduction

The tooz iOS SDK makes it possible to communicate with the tooz glasses. The glasses have a very basic Microprocessor and the following input and output sources:

* **Input**:
  * A touchpad for user interaction
  * An Inertial Measurement Unit (including a magnetometer), henceforth called the **_IMU_**
  * An ambient light sensor to control image brightness, henceforth called the **_ALS_**
  * A proximity sensor for detection whether the device is currently worn on the head
  * Microphone for voice control
* **Output**: OLED, 400 x 640 pixels portrait mode 
* **Connectivity**: Bluetooth Classic and Low energy connectivity
* **Software**: The firmware running on the glasses provides the expected on-device functionalities of the glasses.

## 2. SDK-API

The SDK has three main capabilities:

* Sending images to the glasses
* Receiving button events from the glasses
* Receiving sensor events from the glasses

## 3. Usage

### 3.1 Prerequisites and Usage

The SDK leverages the CoreBluetooth Framework and thus any application using it needs to add the following properties to the plist-file:

* NSBluetoothAlwaysUsageDescription
* NSBluetoothPeripheralUsageDescription (if your app has a deployment target earlier than iOS 13)
* UIBackgroundModes => bluetooth-central (if your app wants to communicate with the glasses while in background)

To add the SDK to your project, there are two options:

**1.) Recommended: Swift Package Manager**

Add this repository as a Swift Package in your Xcode-project by clicking *File => Add Package => Enter the url of this repository in the textfield at the top right corner*. The current version is: **1.0.1**. **Note:** If problems arise with this method, please manually add the SDK (see below).

**2.) Manually add the SDK**

Download the [zipped binary](https://gitlab.com/toozout/bto-sdk/doc-ios-sdk/-/raw/master/tooz-iOS-SDK.xcframework.zip), unzip it and drag the XCFramework-file into your Xcode-Project. 

Then choose your target and under *General -> Frameworks, Libraries, and Embedded Content* choose *Embed & Sign* under the *Embed* tab.


### 3.2 API

#### 3.2.1 Search for devices and connect

    import tooz_iOS_SDK
    import CoreBluetooth
    
    let tooz: ToozBluetooth = Tooz.shared()
    tooz.setConnectivityListener(scanListener: self, connectionStateListener: self)
    tooz.scan(periodic: false)
    
    // In a production app one would normally save all found devices in an array and let the user choose his device
    func onDeviceFound(peripheral: CBPeripheral) {
        tooz.connect(peripheral: peripheral)
    }
    
    
#### 3.2.2 Send an image to the glasses

It is possible to send any UIView to the glasses. It is important that every view that is being sent does have a resolution of exactly 400 x 640 **px** (**not points**). For example, an image view can be sent like this:

    // Views must exactly be 400 x 640 px, this is the resolution of the glasses.
       let imageView = UIImageView(frame: .init(x: 0, y: 0, width: 400 / UIScreen.main.scale, height: 640 / UIScreen.main.scale))
       imageView.image = UIImage(named: imageResource)
                    
    toozBluetooth.sendView(view: imageView, timeToLive: nil, compressionQuality: 0.1)
    
An UITextView can be sent like this:

    let textView = UITextView(frame: .init(x: 0, y: 0, width: 400 / UIScreen.main.scale, height: 640 / UIScreen.main.scale))
    textView.textColor = UIColor(.black)
    textView.textAlignment = NSTextAlignment.center
    textView.text = "This is one awesome textview"
    toozBluetooth.sendView(view: textView, timeToLive: nil, compressionQuality: 0.5)
    
The sendView method has three parameters:

* view: The UIView 
* timeToLive: The duration of time (in milliseconds) the view should be shown. If nil is provided, the image will be shown indefinitely. Note that the display will timeout independently of the timeToLive-paramter.
* The compression: 0 (most) to 1 (least). If nil is provided, the compression is set to 0.5. The higher the compression, the quicker the image will be sent to the glasses. If the image has complex structures, a compression < 0.3 is encourage.d

***Note:*** It is not recommended to send more than 1 frames per second.

#### 3.2.3 Error handling

It is possible that the glasses loose connection with the app that is implementing this SDK. This can happen when the device moves out of range or the glasses are connected with another device among other reasons. The SDK will try to reconnect as soon as possible. One specific error has to be handled manually though: The ConnectionStateListener can receive a connectionError of the type _peerRemovedPairingInformation_. In this case, the user has to manually forget the glasses in the bluetooth settings of the device and then reconnect through the SDK.

### 3.3 Examples
Example Apps that show the general usage of the SDK can be found [here](https://gitlab.com/toozout/bto-sdk/tooz-ios-sdk-examples).

## 4. Using the tooz Emulator

For development purposes, there are two emulators available:

* [iOS-Emulator](https://gitlab.com/toozout/bto-sdk/tooz-ios-emulator)
* [macOS-Emulator](https://gitlab.com/toozout/bto-sdk/tooz-macos-emulator)

Both emulate the basic functionalities of the glasses:

* Sending images (frames)
* Receiving touchpad events

In addition, the iOS-Emulator supports some basic sensor emulation (acceleration etc.).

 Two iPhones **or** an iPhone and a Mac (running at least version 11.0 of macOS) will be needed to start developing right away. Please note that the emulator does not reflect the native glasses behaviour a 100%. The emulators are much slower than the physical glasses and behave differentely in details (for example disconnection events) due to the underlying Bluetooth Low Energy implementations of iOS/macOS. It is therefore discouraged to ship an app to production that has not been tested with the physical glasses. 

## 5. General usage guidelines

Every app integrating this SDK must provide functionality for the following use cases:

* The user must be able to initiate the connection to the glasses
* The user must be able to stop sending images
* The user must be able to disconnect from the glasses

## 6. App Design Implications  

Find general app design implications for the tooz glasses in [**Figma**](https://www.figma.com/file/zTSPZLIkCQqys7PwlT8mNt/tooz-App-Design-Implications-and-Examples?node-id=3%3A6559).

## 7. Troubleshooting

If you have problems with connecting the SDK with the glasses or the emulator, try one of the following or both:

1.) Put the glasses to sleep and wake them up again by folding the temples of the glasses and unfolding them again.  
2.) Put the glasses into pairing mode by Long Tap + Forward Swipe on the glasses touchpad.

If any further problem should arise, feel free to contact us at <dev@tooztech.com>. 

