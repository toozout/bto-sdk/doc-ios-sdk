// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.5 (swiftlang-1300.0.31.1 clang-1300.0.29.1)
// swift-module-flags: -target arm64-apple-ios12.4 -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -module-name tooz_iOS_SDK
import CoreBluetooth
import Foundation
import Swift
import UIKit
import _Concurrency
@_exported import tooz_iOS_SDK
public struct Sensor : Swift.Codable {
  public var name: Swift.String?
  public var reading: tooz_iOS_SDK.SensorReading?
  public func encode(to encoder: Swift.Encoder) throws
  public init(from decoder: Swift.Decoder) throws
}
public struct SensorReading : Swift.Codable {
  public var timeStamp: Swift.Int64?
  public var light: Swift.Double?
  public var proximity: Swift.Bool?
  public var rawProximity: Swift.Int?
  public var temperature: Swift.Double?
  public var acceleration: tooz_iOS_SDK.Acceleration?
  public var gyroscope: tooz_iOS_SDK.Gyroscope?
  public var magneticField: tooz_iOS_SDK.MagneticField?
  public var rotation: tooz_iOS_SDK.Rotation?
  public var gameRotation: tooz_iOS_SDK.GameRotation?
  public var geomagRotation: tooz_iOS_SDK.GeomagRotation?
  public var orientation: tooz_iOS_SDK.Orientation?
  public func encode(to encoder: Swift.Encoder) throws
  public init(from decoder: Swift.Decoder) throws
}
public enum SensorType : Swift.String, Swift.Codable, Swift.CaseIterable {
  case acceleration
  case gameRotation
  case geomagRotation
  case gyroscope
  case light
  case magneticField
  case proximity
  case rawProximity
  public init?(rawValue: Swift.String)
  public typealias AllCases = [tooz_iOS_SDK.SensorType]
  public typealias RawValue = Swift.String
  public static var allCases: [tooz_iOS_SDK.SensorType] {
    get
  }
  public var rawValue: Swift.String {
    get
  }
}
public struct Acceleration : Swift.Codable {
  public var x: Swift.Double?
  public var y: Swift.Double?
  public var z: Swift.Double?
  public func encode(to encoder: Swift.Encoder) throws
  public init(from decoder: Swift.Decoder) throws
}
public struct Gyroscope : Swift.Codable {
  public var x: Swift.Double?
  public var y: Swift.Double?
  public var z: Swift.Double?
  public func encode(to encoder: Swift.Encoder) throws
  public init(from decoder: Swift.Decoder) throws
}
public struct MagneticField : Swift.Codable {
  public var x: Swift.Double?
  public var y: Swift.Double?
  public var z: Swift.Double?
  public func encode(to encoder: Swift.Encoder) throws
  public init(from decoder: Swift.Decoder) throws
}
public struct Rotation : Swift.Codable {
  public var x: Swift.Double?
  public var y: Swift.Double?
  public var z: Swift.Double?
  public var w: Swift.Double?
  public func encode(to encoder: Swift.Encoder) throws
  public init(from decoder: Swift.Decoder) throws
}
public struct GameRotation : Swift.Codable {
  public var x: Swift.Double?
  public var y: Swift.Double?
  public var z: Swift.Double?
  public var w: Swift.Double?
  public func encode(to encoder: Swift.Encoder) throws
  public init(from decoder: Swift.Decoder) throws
}
public struct GeomagRotation : Swift.Codable {
  public var x: Swift.Double?
  public var y: Swift.Double?
  public var z: Swift.Double?
  public var w: Swift.Double?
  public func encode(to encoder: Swift.Encoder) throws
  public init(from decoder: Swift.Decoder) throws
}
public struct Orientation : Swift.Codable {
  public var x: Swift.Double?
  public var y: Swift.Double?
  public var z: Swift.Double?
  public func encode(to encoder: Swift.Encoder) throws
  public init(from decoder: Swift.Decoder) throws
}
public enum ButtonCode : Swift.Int, Swift.Codable {
  case unknown
  case a_1s
  case a_1l
  case a_xl
  case b_1s
  case b_1l
  case b_xl
  case ab_1s
  case ab_1l
  case a_2s
  case b_2s
  case ab_xl
  case ab_2s
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
public protocol ScanListener {
  func onDeviceFound(peripheral: CoreBluetooth.CBPeripheral)
}
public enum ConnectionState {
  case connected(peripheral: CoreBluetooth.CBPeripheral, batteryLevel: Swift.Int)
  case disconnected
  case connectionError(cause: tooz_iOS_SDK.ConnectionErrorCause)
  case bluetoothReady
}
public enum ConnectionErrorCause : Swift.Int, Swift.Identifiable {
  public var id: Swift.Int {
    get
  }
  case peerRemovedPairingInformation
  case failedToEncryptConnection
  case deviceHasDisconnected
  case unknownDisconnectionReason
  case connectionAttemptTimeout
  case connectionTimeOutUnexpectedly
  case authenticationIsInsufficient
  public init?(rawValue: Swift.Int)
  public typealias ID = Swift.Int
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
public protocol ConnectionStateListener {
  func onConnectionStateChanged(connectionState: tooz_iOS_SDK.ConnectionState)
}
public struct SensorOfInterest : Swift.Codable, Swift.Hashable {
  public let name: tooz_iOS_SDK.SensorType
  public let delay: Swift.Int
  public init(name: tooz_iOS_SDK.SensorType, delay: Swift.Int)
  public func hash(into hasher: inout Swift.Hasher)
  public static func == (a: tooz_iOS_SDK.SensorOfInterest, b: tooz_iOS_SDK.SensorOfInterest) -> Swift.Bool
  public func encode(to encoder: Swift.Encoder) throws
  public var hashValue: Swift.Int {
    get
  }
  public init(from decoder: Swift.Decoder) throws
}
@_hasMissingDesignatedInitializers public class Tooz {
  public static func shared() -> tooz_iOS_SDK.ToozBluetooth
  public static func reset()
  @objc deinit
}
public protocol ToozBluetooth {
  func setConnectivityListener(scanListener: tooz_iOS_SDK.ScanListener, connectionStateListener: tooz_iOS_SDK.ConnectionStateListener)
  func scan(periodic: Swift.Bool)
  func stopScan()
  func connect(peripheral: CoreBluetooth.CBPeripheral)
  func disconnect()
  func sendView(view: UIKit.UIView, timeToLive: Swift.Int?, compressionQuality: CoreGraphics.CGFloat?)
  func sendView(view: Foundation.Data, timeToLive: Swift.Int?)
  func setButtonEventListener(_ buttonEventListener: tooz_iOS_SDK.ButtonEventListener)
  func setSensorDataListener(sensorDataListener: tooz_iOS_SDK.SensorDataListener?, sensorsOfInterest: Swift.Set<tooz_iOS_SDK.SensorOfInterest>?)
  func shouldAutoReconnect(_ should: Swift.Bool)
}
public protocol ButtonEventListener {
  func onButtonEventReceived(_ buttonCode: tooz_iOS_SDK.ButtonCode)
}
public protocol SensorDataListener {
  func onSensorDataReceived(_ sensors: [tooz_iOS_SDK.Sensor])
}
extension tooz_iOS_SDK.SensorType : Swift.Equatable {}
extension tooz_iOS_SDK.SensorType : Swift.Hashable {}
extension tooz_iOS_SDK.SensorType : Swift.RawRepresentable {}
extension tooz_iOS_SDK.ButtonCode : Swift.Equatable {}
extension tooz_iOS_SDK.ButtonCode : Swift.Hashable {}
extension tooz_iOS_SDK.ButtonCode : Swift.RawRepresentable {}
extension tooz_iOS_SDK.ConnectionErrorCause : Swift.Equatable {}
extension tooz_iOS_SDK.ConnectionErrorCause : Swift.Hashable {}
extension tooz_iOS_SDK.ConnectionErrorCause : Swift.RawRepresentable {}
