### Core-Library

This library contains core functionalities for implementing the tooz glass protocol via ble on iOS-Devices. This library is used both in the iOS-Emulator and the iOS-SDK.